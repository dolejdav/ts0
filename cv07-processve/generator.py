from copy import copy

def load_graph_file(path):
    start = "START"
    end = "END"
    V = []
    E = []
    end_id = ""
    temp_E_in = {}
    temp_E_out = {}
    with open(path) as f:
        for line in f:
            tokens = line.strip().split("\t")
            V.append(tokens[0])
            e_in = tokens[1].split(";")
            e_out = tokens[2].split(";")
            for id in e_in:
                temp_E_in[id] = tokens[0]
            for id in e_out:
                temp_E_out[id] = tokens[0]
    for id, vout in temp_E_out.items():
        if id in temp_E_in:
            vin = temp_E_in[id]
            E.append((id, vout, vin))
        else:
            end_id = id
    V.append(start)
    V.append(end)
    E.append(("1", start, temp_E_in["1"]))
    E.append((end_id, temp_E_out[end_id], end))
    return (V, E)

def traverse(G):
    out = []
    a = []
    a.append(("START", [], {}))
    while len(a) > 0:
        new_a = []
        for x in a:
            e = filter(lambda y: y[1] == x[0] and (y[0] not in x[2] or x[2][y[0]] <= 1), G[1])
            for y in e:
                if y[2] == "END":
                    out.append(x[1] + [y[0]])
                    continue
                new_dict = copy(x[2])
                if y[0] not in new_dict:
                    new_dict[y[0]] = 0
                new_dict[y[0]] += 1
                new_a.append((y[2], x[1][:] + [y[0]], new_dict))
        a = new_a
    return out
    

def contains(s, c):
    for i in range(len(s) - len(c) + 1):
        eq = True
        for j in range(len(c)):
            if s[i + j] != c[j]:
                eq = False
                break
        if eq:
            return True
    return False

def remove_duplicit(S):
    to_remove = []
    for i in range(len(S)):
        a = S[i]
        for j in range(len(S)):
            is_duplicit = True
            if i == j:
                continue
            b = S[j]
            for k in range(len(a) - 1):
                if not contains(b, a[k:k+2]):
                    is_duplicit = False
            if is_duplicit:
                to_remove.append(a)
                break
    return list(filter(lambda x: x not in to_remove, S))

def process(path):
    G = load_graph_file(path)
    S = traverse(G)
    result = remove_duplicit(S)
    with open(path + "_out.csv", "w") as f:
        f.write("\n".join([str(i) + ";" + " - ".join(result[i]) for i in range(len(result))]))

process("input")
     