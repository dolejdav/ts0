package cz.cvut.fel;

import cz.cvut.fel.pages.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.List;

public class App {
    private WebDriver driver;
    private WebDriverWait wait;

    public void run() {
        start();

        try {
            MainPage mainPage = new MainPage(driver, wait);
            LoginPage loginPage = mainPage.navigateToLoginPage();
            loginPage.loginAs("dixogas402@rencr.com");
            // Navigate to log in password insertion\
            loginPage.loginWithPassword("hackerhacker");
            // Navigate to advanced search page and perform the search
            AdvancedSearchPage advancedSearchPage = mainPage.navigateToAdvancedSearchPage();
            SearchResultPage searchResultsPage = advancedSearchPage.performSearch("Page Object Model ","Selenium Testing", "2024"); // Adjust the search query and year as necessary

            // Extract and print the first four articles' details
            List<ArticleInfo> articles = searchResultsPage.extractFirstFourArticles();
            for (ArticleInfo article : articles) {
                System.out.println("Title: " + article.title() + ", DOI: " + article.doi() + ", Publication Date: " + article.publicationDate());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        destroy();
    }

    public void start() {
        driver = new FirefoxDriver();
        wait = new WebDriverWait(driver, Duration.of(5, ChronoUnit.SECONDS));
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.get("https://link.springer.com/");
    }

    public void destroy() {
        driver.quit();
    }

    public WebDriver getDriver() {
        return driver;
    }

    public WebDriverWait getWait() {
        return wait;
    }
}
