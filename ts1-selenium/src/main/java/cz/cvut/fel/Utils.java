package cz.cvut.fel;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Utils {
    public static void dismissCookieConsentIfPresent(WebDriver driver, WebDriverWait wait) {
        try {
            // CSS Selector for the cookie consent 'Accept' button
            By cookieConsentSelector = By.xpath("/html/body/section/div/div[2]/button[1]");

            // Wait for the cookie consent banner button to be clickable
            WebElement cookieConsentButton = wait.until(ExpectedConditions.elementToBeClickable(cookieConsentSelector));

            // Click the cookie consent button to accept and close the banner
            cookieConsentButton.click();

            System.out.println("Cookie consent banner was present and has been dismissed.");
        } catch (NoSuchElementException | TimeoutException e) {
            // If the element is not found or not clickable within the wait time, assume it's not present
            System.out.println("Cookie consent banner not present or not clickable at this time.");
        }
    }
}
