package cz.cvut.fel.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class SearchResultPage extends Page{
    // Použijte jeden seznam pro všechny odkazy na články
    @FindBy(xpath = "//*[@id=\"main\"]/div/div[3]/div/div[2]/div[2]/ol/li")
    private List<WebElement> articleLinks;

    @FindBy(css = "#article-info-content > div > div:nth-child(2) > ul.c-bibliographic-information__list > li.c-bibliographic-information__list-item.c-bibliographic-information__list-item--full-width > p > span.c-bibliographic-information__value")
    private WebElement articleDOIs;

    @FindBy(xpath = "//ul[contains(@class, 'c-bibliographic-information__list')]/li[p[contains(text(), 'Published')]]//time")
    private WebElement articlePublishedDates;

    @FindBy(xpath = "//*[@id=\"main\"]/section/div/div/div[1]/h1")
    private WebElement articleTitles;

    @FindBy(xpath = "//span[text()='Article']")
    private WebElement articleCheckbox;

    @FindBy(xpath = "//*[@id=\"popup-filters\"]/div[3]/button[2]")
    private WebElement updateSearchResults;

    public SearchResultPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public void getArticleCheckBoxOptionAndUpdate() {
        wait.until(ExpectedConditions.elementToBeClickable(articleCheckbox));
        articleCheckbox.click();
        wait.until(ExpectedConditions.elementToBeClickable(updateSearchResults));
        updateSearchResults.click();
        //BaseClass.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"main\"]/div/div[3]/div/div[2]/div[2]/ol/li[1]/div[1]/div/h3/a"))); // Čekání na načtení článků

    }

    public List<ArticleInfo> extractFirstFourArticles() {
        getArticleCheckBoxOptionAndUpdate();
        List<ArticleInfo> articles = new ArrayList<>();
        int count = Math.min(4, articleLinks.size());

        if (articleLinks.isEmpty()) {
            System.out.println("No articles found. Check if the page has loaded correctly and the XPaths are correct.");
            return articles;
        }

        for (int i = 0; i < count; i++) {
            articleLinks.get(i).click();  // Click on the article title link
            getTitle();
            getDOI();
            getPublishedDate();

            articles.add(new ArticleInfo(getTitle(), getDOI(), getPublishedDate()));

            driver.navigate().back();  // Go back to the list of articles
            wait.until(ExpectedConditions.visibilityOf(articleLinks.get(0)));  // Ensure the list is visible again
        }
        return articles;
    }


    public List<ArticleInfo> getArticles() {
        return this.extractFirstFourArticles();
    }

    public String getDOI() {
        return articleDOIs.getText();
    }

    public String getTitle() {
        return articleTitles.getText();
    }

    public String getPublishedDate() {
        return articlePublishedDates.getText();
    }
}
