package cz.cvut.fel.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;

public class MainPage extends Page {
    @FindBy(xpath = "//*[@id=\"identity-account-widget\"]")
    private WebElement loginLink;

    @FindBy(xpath = "//a[contains(text(),'Advanced Search')]")
    private WebElement advancedSearchLink;

    public MainPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public LoginPage navigateToLoginPage() {
        try {
            // Attempt to dismiss common overlays or banners
            WebElement consentDismiss = driver.findElement(By.cssSelector("body > dialog > div.cc-banner__content > div > div.cc-banner__footer > button.cc-button.cc-button--secondary.cc-button--contrast.cc-banner__button.cc-banner__button-accept"));
            consentDismiss.click();
        } catch (Exception e) {
            System.out.println("No consent banner found or not dismissible.");
        }

        // Wait until the login link is clickable and then click
        wait.until(elementToBeClickable(loginLink));
        loginLink.click();

        return new LoginPage(driver, wait);
    }

    public AdvancedSearchPage navigateToAdvancedSearchPage() {
        driver.get("https://link.springer.com/advanced-search");

        return new AdvancedSearchPage(driver, wait);
    }
}
