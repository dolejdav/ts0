package cz.cvut.fel.pages;

import cz.cvut.fel.Utils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AdvancedSearchPage extends Page {
    @FindBy(xpath = "//*[@id=\"all-words\"]")
    private WebElement allWordSearchBox;

    @FindBy(xpath = "//*[@id=\"least-words\"]")
    private WebElement someWordSearchBox;

    @FindBy(xpath = "//*[@id=\"date-facet-mode\"]")
    private WebElement searchTypeDropdown;

    @FindBy(xpath = "//*[@id=\"facet-start-year\"]")
    private WebElement enterYear;

    @FindBy(xpath = "//*[@id=\"submit-advanced-search\"]")
    private WebElement searchButton;

    public AdvancedSearchPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
        Utils.dismissCookieConsentIfPresent(driver, wait);
    }

    public void enterSearchQuery1(String query) {
        wait.until(ExpectedConditions.visibilityOf(allWordSearchBox));
        allWordSearchBox.sendKeys(query);
        //someWordSearchBox.sendKeys(query);
    }

    public void enterSearchQuery2(String query) {
        wait.until(ExpectedConditions.visibilityOf(someWordSearchBox));
        someWordSearchBox.sendKeys(query);
    }

    public void selectSearchType(String type) {
        wait.until(ExpectedConditions.elementToBeClickable(searchTypeDropdown));
        if (searchTypeDropdown.isEnabled()) {
            Select selectEle = new Select(searchTypeDropdown);
            selectEle.selectByVisibleText("in");
            System.out.println("Select element is enabled.");
        } else {
            System.out.println("Select element is not enabled.");
        }
        enterYear.sendKeys(type);
    }

    public void clickSearchButton() {
        wait.until(ExpectedConditions.elementToBeClickable(searchButton));
        searchButton.click();
    }

    public SearchResultPage performSearch(String query1, String query2,String type) {
        enterSearchQuery1(query1);
        enterSearchQuery2(query2);
        selectSearchType(type);
        clickSearchButton();
        return new SearchResultPage(driver, wait);
    }
}
