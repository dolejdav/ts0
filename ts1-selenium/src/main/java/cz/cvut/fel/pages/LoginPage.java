package cz.cvut.fel.pages;

import cz.cvut.fel.Utils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage extends Page{
    @FindBy(id = "username")
    private WebElement usernameField;

    @FindBy(xpath = "//*[@id=\"login-password\"]")
    private WebElement passwordField;

    @FindBy(xpath = "//*[@id=\"login-email\"]")
    private WebElement emailField;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement loginButton;

    @FindBy(id = "password-submit")
    private WebElement continueButton;

    public LoginPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
        Utils.dismissCookieConsentIfPresent(driver, wait);
    }

    public void enterUsername(String username) {
        wait.until(ExpectedConditions.visibilityOf(usernameField));
        usernameField.sendKeys(username);
    }

    public void enterPassword(String password) {
        wait.until(ExpectedConditions.visibilityOf(passwordField));
        passwordField.sendKeys(password);
    }

    public void enterEmail(String email) {
        wait.until(ExpectedConditions.visibilityOf(emailField));
        emailField.sendKeys(email);
    }

    public void clickLoginButton() {
        wait.until(ExpectedConditions.elementToBeClickable(loginButton));
        loginButton.click();
    }

    public void clickContinueButton() {
        wait.until(ExpectedConditions.elementToBeClickable(continueButton));
        continueButton.click();

    }

    public void loginAs(String email) {
        enterEmail(email);
        clickLoginButton();
    }

    public void loginWithPassword(String password) {
        enterPassword(password);
        clickLoginButton();
    }
}
