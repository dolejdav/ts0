package cz.cvut.fel.ts1.shop;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import static org.junit.jupiter.api.Assertions.*;

public class StandardItemTest {
    private StandardItem standardItem;

    @BeforeEach
    public void setUp() {
        standardItem = new StandardItem(1, "Name", 1000, "CATEGORY", 10);
    }

    @Test
    @DisplayName("Testing StandardItem constructor")
    public void testStandardItemConstructor() {

        assertEquals(1, standardItem.getID());
        assertEquals("Name", standardItem.getName());
        assertEquals(1000, standardItem.getPrice());
        assertEquals("CATEGORY", standardItem.getCategory());
        assertEquals(10, standardItem.getLoyaltyPoints());
    }

    @Test
    @DisplayName("Testing copy() method of StandardItem class")
    public void testCopyMethod() {
        StandardItem standardItemCopy = standardItem.copy();

        assertEquals(1, standardItemCopy.getID());
        assertEquals("Name", standardItemCopy.getName());
        assertEquals(1000, standardItemCopy.getPrice());
        assertEquals("CATEGORY", standardItemCopy.getCategory());
        assertEquals(10, standardItemCopy.getLoyaltyPoints());
    }

    @ParameterizedTest
    @DisplayName("Testing equals() method of StandardItem class")
    @CsvFileSource(resources = "/parametersForStandardItemEqualsMethod.csv", numLinesToSkip = 1)
    public void testEqualsMethod(int id1, String name1, float price1, String category1, int loyaltyPoints1,
                                 int id2, String name2, float price2, String category2, int loyaltyPoints2,
                                 boolean expectedResult) {
        StandardItem si1 = new StandardItem(id1, name1, price1, category1, loyaltyPoints1);
        StandardItem si2 = new StandardItem(id2, name2, price2, category2, loyaltyPoints2);

        assertEquals(expectedResult, si1.equals(si2), "The equals method should return true if the same object");
    }
}
