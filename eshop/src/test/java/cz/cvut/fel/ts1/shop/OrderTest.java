package cz.cvut.fel.ts1.shop;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class OrderTest {
    private ShoppingCart cart;

    @BeforeEach
    public void setUp() {
        cart = new ShoppingCart();
    }

    @Test
    @DisplayName("Testing Order constructor")
    public void testOrderConstructor() {
        Order order = new Order(cart, "Firstname Surname", "123 Wall Street", 1);

        assertEquals(cart.getCartItems(), order.getItems(), "Items should be the same");
        assertEquals("Firstname Surname", order.getCustomerName(), "Customer name should be the same");
        assertEquals("123 Wall Street", order.getCustomerAddress(), "Customer address should be the same");
        assertEquals(1, order.getState(), "State should be the same");
    }

    @Test
    @DisplayName("Testing Order constructor with default state")
    public void testOrderConstructorWithDefaultState() {
        Order order = new Order(cart, "Firstname Surname", "123 Wall Street");

        assertEquals(cart.getCartItems(), order.getItems(), "Items should be the same");
        assertEquals("Firstname Surname", order.getCustomerName(), "Customer name should be the same");
        assertEquals("123 Wall Street", order.getCustomerAddress(), "Customer address should be the same");
        assertEquals(0, order.getState(), "State should be default state (0)");
    }

    @Test
    @DisplayName("Testing Order constructor with null cart")
    public void testOrderConstructorWithNullCart() {
        Exception e = assertThrows(IllegalArgumentException.class, () -> new Order(null, "Firstname Surname", "123 Wall Street", 1));

        String expectedMessage = "Cart cannot be null";
        String actualMessage = e.getMessage();
        assertTrue(actualMessage.contains(expectedMessage), "Message should contain " + expectedMessage);
    }

    @Test
    @DisplayName("Testing Order constructor with null customer details")
    public void testOrderConstructorWithNullCustomerDetails() {
        Order order = new Order(cart, null, null, 1);

        assertNull(order.getCustomerName(), "Customer name should be null");
        assertNull(order.getCustomerAddress(), "Customer address should be null");
    }

}
