package cz.cvut.fel.ts1.archive;

import cz.cvut.fel.ts1.shop.Item;
import cz.cvut.fel.ts1.shop.Order;
import cz.cvut.fel.ts1.shop.StandardItem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PurchasesArchiveTest {
    private PurchasesArchive archive;
    private Order orderMock;
    private StandardItem itemMock;

    @BeforeEach
    public void setUp() {
        archive = new PurchasesArchive();
        orderMock = mock(Order.class);
        itemMock = mock(StandardItem.class);

        ArrayList<Item> items = new ArrayList<>(Collections.nCopies(3, itemMock));

        when(orderMock.getItems()).thenReturn(items);
        when(itemMock.getID()).thenReturn(1);
    }

    @Test
    public void testPutOrderToPurchasesArchive() {

        archive.putOrderToPurchasesArchive(orderMock);

        assertEquals(3, archive.getHowManyTimesHasBeenItemSold(itemMock));
    }

    @Test
    public void testGetHowManyTimesHasBeenItemSold() {

        archive.putOrderToPurchasesArchive(orderMock);
        int soldTimes = archive.getHowManyTimesHasBeenItemSold(itemMock);

        assertEquals(3, soldTimes);
    }

    @Test
    public void testPrintItemPurchaseStatistics() {

        archive.putOrderToPurchasesArchive(orderMock);
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();

        System.setOut(new PrintStream(outContent));
        archive.printItemPurchaseStatistics();

        assertTrue(outContent.toString().contains("ITEM PURCHASE STATISTICS:"));
        assertTrue(outContent.toString().contains("HAS BEEN SOLD 3 TIMES"));
        System.setOut(System.out);
    }

    @Test
    public void testItemPurchaseArchiveEntryConstructor() {
        Item testItem = mock(Item.class);
        when(testItem.getID()).thenReturn(2);
        when(testItem.toString()).thenReturn("Test Item");

        ItemPurchaseArchiveEntry archiveEntry = new ItemPurchaseArchiveEntry(testItem);

        assertEquals(1, archiveEntry.getCountHowManyTimesHasBeenSold());
        assertEquals("ITEM  Test Item   HAS BEEN SOLD 1 TIMES", archiveEntry.toString());
    }
}
