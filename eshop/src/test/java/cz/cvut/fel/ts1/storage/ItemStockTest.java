package cz.cvut.fel.ts1.storage;

import cz.cvut.fel.ts1.shop.StandardItem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ItemStockTest {
    private StandardItem standardItem;

    @BeforeEach
    public void setUp() {
        standardItem = new StandardItem(1, "Name", 1000, "CATEGORY", 10);
    }

    @Test
    @DisplayName("Testing ItemStock constructor")
    public void testItemStockConstructor() {
        ItemStock itemStock = new ItemStock(standardItem);

        assertEquals(standardItem, itemStock.getItem());
        assertEquals(0, itemStock.getCount());
    }

    @ParameterizedTest
    @ValueSource(ints = { -1, 0, 1, 5 })
    @DisplayName("Testing IncreaseItemCount() method of ItemStock class")
    public void testIncreaseItemCountMethod(int increment) {
        ItemStock itemStock = new ItemStock(standardItem);

        if(increment <= 0) {
            assertThrows(IllegalArgumentException.class, () -> itemStock.IncreaseItemCount(increment));
        } else {
            itemStock.IncreaseItemCount(increment);
            assertEquals(increment, itemStock.getCount(), "Increase Item Count failed");
        }
    }

    @ParameterizedTest
    @ValueSource(ints = { -1, 0, 1, 5 })
    @DisplayName("Testing DecreaseItemCount() method of ItemStock class")
    public void testDecreaseItemCountMethod(int decrement) {
        ItemStock itemStock = new ItemStock(standardItem);

        itemStock.IncreaseItemCount(20);

        if(decrement <= 0) {
            assertThrows(IllegalArgumentException.class, () -> itemStock.decreaseItemCount(decrement));
        } else {
            itemStock.decreaseItemCount(decrement);
            assertEquals(20 - decrement, itemStock.getCount(), "Decrease Item Count failed");
        }
    }
}
