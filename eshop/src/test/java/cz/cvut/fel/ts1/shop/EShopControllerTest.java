package cz.cvut.fel.ts1.shop;

import cz.cvut.fel.ts1.archive.PurchasesArchive;
import cz.cvut.fel.ts1.storage.NoItemInStorage;
import cz.cvut.fel.ts1.storage.Storage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class EShopControllerTest {
    private PurchasesArchive archive;
    private Storage storage;
    private ShoppingCart cart;

    @BeforeEach
    public void setUp() {
        cart = new ShoppingCart();
        archive = mock(PurchasesArchive.class);
        storage = mock(Storage.class);
        EShopController.setStorage(storage);
        EShopController.setArchive(archive);
    }

    @Test
    @DisplayName("Testing successful purchase")
    public void testSuccessfulPurchase() {
        try {
            cart.addItem(new StandardItem(1, "Name", 1000, "CATEGORY", 10));
            assertEquals(1, cart.getItemsCount());

            cart.addItem(new DiscountedItem(2, "Name", 1000, "CATEGORY", 10, "1.1.2025", "1.1.2030"));
            assertEquals(2, cart.getItemsCount());

            cart.removeItem(1);
            assertEquals(1, cart.getItemsCount());

            assertEquals(900, cart.getTotalPrice());

            EShopController.purchaseShoppingCart(cart, "Firstname Surname", "123 Wall Street");

            verify(storage).processOrder(any(Order.class));
            verify(archive).putOrderToPurchasesArchive(any(Order.class));

        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    @DisplayName("Testing purchase when out of stock")
    public void testOutOfStockPurchase() {
        try {
            cart.addItem(new StandardItem(1, "Name", 1000, "CATEGORY", 10));
            assertEquals(1, cart.getItemsCount());

            assertEquals(1000, cart.getTotalPrice());

            doThrow(new NoItemInStorage()).when(storage).processOrder(any(Order.class));

            assertThrows(NoItemInStorage.class, () -> EShopController.purchaseShoppingCart(cart, "Firstname Surname", "123 Wall Street"));

        } catch (Exception e) {
            fail(e);
        }
    }
}
