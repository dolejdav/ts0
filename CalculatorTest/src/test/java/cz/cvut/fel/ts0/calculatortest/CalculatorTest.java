package cz.cvut.fel.ts0.calculatortest;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CalculatorTest {
    private Calculator calculator;

    @BeforeEach
    public void init() {
        calculator = new Calculator();
    }

    @Test
    public void add_addThreeAndTwo_five() {
        assertEquals(5, calculator.add(3, 2));
    }

    @Test
    public void sub_subtractTwoFromThree_one() {
        assertEquals(1, calculator.sub(3, 2));
    }

    @Test
    public void div_divideTwentyByFive_four() {
        assertEquals(4, calculator.div(20, 5));
    }

    @Test
    public void div_divideNineteenByFive_three() {
        assertEquals(3, calculator.div(19, 5));
    }

    @Test
    public void div_divideFiveByZero_throwsException() {
        Exception exception = assertThrows(ArithmeticException.class, () -> {
            calculator.div(5, 0);
        });

        String expectedMessage = "/ by zero";
        String actualMessage = exception.getMessage();

        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void mul_threeAndTwo_six() {
        assertEquals(6, calculator.mul(3, 2));
    }
}
